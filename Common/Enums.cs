﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Common
{
    class Enums
    {
        public enum Genero
        {
            [Display(Name="Femenino")]
            FEMENINO,
            [Display(Name = "Masculino")]
            MASCULINO
        }
    }
}
