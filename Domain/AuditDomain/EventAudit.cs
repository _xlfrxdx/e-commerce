﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.AuditDomain
{
    [Table("EventAudit")]
    public class EventAudit
    {
        [Key]
        public int EventId { get; set; }
        public string TargetType { get; set; }
        public string EventType { get; set; }
        public string TargetOld { get; set; }
        public string TargetNew { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Duration { get; set; }
        public string Domain { get; set; }
        public string JsonData { get; set; }
    }
}
