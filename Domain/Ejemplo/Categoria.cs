﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Ejemplo
{
    [Table("Categorias")]
    public class Categoria
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "El campo 'Nombre' es requerido")]
        [StringLength(20)]
        public string Nombre { get; set; }

        [StringLength(150)]
        public string Descripcion { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime RowVersion { get; set; }
        public string AppUser { get; set; }
        public Collection<Producto> productos { get; set; }
        ///[ForeignKey]


    }
}
