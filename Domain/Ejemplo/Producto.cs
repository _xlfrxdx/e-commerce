﻿using Domain.Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Domain.Ejemplo
{
    public class Producto
    {
        [Key]
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        [ForeignKey("Categoria")]
        public int IdCategoria { get; set; }
        public float precio { get; set; }
        public int talla_XS { get; set; }
        public int talla_S { get; set; }
        public int talla_M { get; set; }
        public int talla_L { get; set; }
        public int PorcentajeDes { get; set; }
        public int ContadorVisitas { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime RowVersion { get; set; }
        public string AppUser { get; set; }

        public virtual ICollection<ImagenProducto> ImagenProducto { get; set; }

        public virtual Categoria Categoria { get; set; }


        [NotMapped]
        public int stok
        {
            get
            {
                return this.talla_L + this.talla_M + this.talla_S + this.talla_XS;
            }
        }
        [NotMapped]
        public bool Descuento
        {
            get
            {
                if (this.PorcentajeDes == 0 )
                {
                    return false;
                }
                return true;
            }
        }

        [NotMapped]
        public double PrecioConDescuento
        {
            get
            {
                double pcd = this.precio * Math.Abs((this.PorcentajeDes / 100.0) - 1);

                return pcd;
            }
        }

        [NotMapped]
        public string ImagenPrincipal
        {
            get
            {
                string imgP = (this.ImagenProducto != null ? this.ImagenProducto.FirstOrDefault().URL : "");

                return imgP;
            }
        }

    }
}
