﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Ejemplo
{
    [Table("Direcciones")]
    public class Dirrecion
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string CP { get; set; }
        public string Ciudad { get; set; }
        [Required]
        public string Colonia { get; set; }
        public string Calle { get; set; }
        public string NumeroInterior { get; set; }
        public string NumeroExterior { get; set; }
        [Required]
        public string Latitud { get; set; }
        [Required]
        public string Longitud { get; set; }
        public bool IsDeleted { get; set; }

        public string ObtenerDireccion()
        {
            return Ciudad + " Colonia " + Colonia + " Calle " + Calle + ", CP " + CP;
        }
    }
}
