﻿using Domain.Objetos;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Ejemplo
{
    public class DirrecionPersona : Dirrecion
    {
        [ForeignKey("Persona")]
        public int? IdPersona { get; set; }
        public virtual Persona Persona { get; set; }
    }
}
