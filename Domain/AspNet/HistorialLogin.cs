﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.AspNet
{
    [Table("historialLogin")]
   public class HistorialLogin
    {
        [Key]
        public int Id { get; set; }
        public string Username { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime Fecha { get; set; }
        [MaxLength(100)]
        public string Acceso { get; set; }
        public string Proyecto { get; set; }
        public string DireccionIP { get; set; }
        public bool IsDeleted { get; set; }
    
    }
}
