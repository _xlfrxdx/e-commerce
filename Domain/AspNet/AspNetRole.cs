﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.AspNet
{
    public class AspNetRole: IdentityRole
    {
        public bool IsDeleted {get; set;}
    }
}
