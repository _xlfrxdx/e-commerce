﻿using Domain.Objetos;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.AspNet
{
   public class ApplicationUser : IdentityUser
    {
        public Persona Persona { get; set; }
        public bool IsDeleted { get;  set; }

    }
}
