﻿using Domain.Ejemplo;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ViewModel
{
   public class ListaProductoViewModel
    {
        public List<Producto> ObtenerListaPDescuento { get; set; }
        public List<Producto> ObtenerListaPRecientes { get; set; }
        public List<Producto> ObtenerListaPMasVistos { get; set; }
    }
}
