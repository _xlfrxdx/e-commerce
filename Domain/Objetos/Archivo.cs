﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Objetos
{
    public abstract class Archivo
    {
        public Archivo()
        {
            FechaDeAlta = DateTime.Now;
        }
        [Key]
        public int Id { get; set; }
        public DateTime? FechaDeAlta { get; set; }

        public string Nombre { get; set; }
        public string URL { get; set; }
        public bool Estatus { get; set; }

        public bool IsDeleted { get; set; }
        public DateTime? RowVersion { get; set; }
        public string AppUser { get; set; }
    }
}
