﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Objetos
{
    [Table("Ventas")]
    public class Venta
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Persona")]
        public int IdUsuario { get; set; }
        public string ListaProductos { get; set; }
        public float TotalPagar { get; set; }
        public float SubTotal { get; set; }
        public float IVA { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime RowVersion { get; set; }
        public string AppUser { get; set; }
        public virtual Persona Persona { get; set; }

       
    }
}
