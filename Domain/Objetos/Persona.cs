﻿using Domain.AspNet;
using Domain.Ejemplo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Objetos
{
    [Table("Persona")]
    public class Persona 
    {
        public Persona()
        {
            RowVersion = DateTime.Now;
        }
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage ="El campo 'Nombre' es obligatorio")]
        [StringLength(maximumLength:3)]
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        [ForeignKey("ApplicationUser")]
        public string IdApplicationUser { get; set; }
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual ICollection<DirrecionPersona> Dirreciones { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? RowVersion { get; set; }
        public string AppUser { get; set; }
        public virtual ICollection<Venta> Ventas { get; set; }
        //
        [NotMapped]
        public string NombreCompleto
        {
            get
            {
                return this.Nombre + " " + this.Apellidos;
            }
        }

    }
}
