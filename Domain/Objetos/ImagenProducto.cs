﻿using Domain.Ejemplo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Objetos
{
   public class ImagenProducto : Archivo
    {
        [ForeignKey("ProductoImg")]
        public int? IdProducto { get; set; }
        public int Contador { get; set; }
        public virtual Producto ProductoImg { get; set; }
    }
}
