﻿using Domain.AspNet;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositorios.Seguridad
{
   
    public class HistorialLoginRepositorio
    {
        private readonly ApplicationDbContext _context;
        public HistorialLoginRepositorio(ApplicationDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Query que se encarga de crear un registro
        /// </summary>
        /// <param name="historialLogin"></param>
        /// <returns>
        /// esta query retorna un int
        /// </returns>
        public async Task<int> Crear(HistorialLogin historialLogin)
        {

            await _context.HistorialLogins.AddAsync(historialLogin);
            return await _context.SaveChangesAsync();
        }
    }
}
