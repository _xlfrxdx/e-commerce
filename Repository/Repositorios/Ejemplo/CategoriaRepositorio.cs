﻿using Domain.Ejemplo;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositorios.Ejemplo
{
    public class CategoriaRepositorio
    {
        private readonly ApplicationDbContext _context;

        public CategoriaRepositorio(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<List<Categoria>> obtenerLista()
        {
            return await _context.Categorias.ToListAsync();
        }
        public async Task<int> crear(Categoria categoria)
        {
            await _context.Categorias.AddAsync(categoria);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> editar(Categoria categoria)
        {
             _context.Categorias.Update(categoria);
             return await _context.SaveChangesAsync();
        }
        public async Task<int> Eliminar(int id)
        {
            Categoria categoria = new Categoria();
            //categoria = await _context.Categorias.FindAsync(id);
            categoria = await _context.Categorias.FirstOrDefaultAsync(x => x.Id == id);
            _context.Remove(categoria);
            return await _context.SaveChangesAsync();
        }
        public async Task<Categoria> buscarPorId(int? id)
        {
            return await _context.Categorias.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<bool> ExisteNombreDiferenteId(int? id, string nombre)
        {
            return await _context.Categorias.AnyAsync(x => x.Id != id && x.Nombre == nombre);
        }

    }
}
