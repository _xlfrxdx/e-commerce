﻿using Domain.Objetos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositorios
{
    public class ImagenProductoRepositorio
    {
        private readonly ApplicationDbContext _context;
        public ImagenProductoRepositorio( ApplicationDbContext dbContext )
        {
            _context = dbContext;
        }
        public async Task<int> Crear(ImagenProducto imagenProducto)
        {
            await _context.ImagenProductos.AddAsync(imagenProducto);
            return await  _context.SaveChangesAsync();
        }
        public async Task<int> CrearLista(List<ImagenProducto> ListaImagen)
        {
            await _context.ImagenProductos.AddRangeAsync(ListaImagen);
            return await _context.SaveChangesAsync();
        }
        public async Task<List<ImagenProducto>> BuscarImagenXPorducto(int id)
        {
            return await _context.ImagenProductos.Where(x => x.IdProducto == id).ToListAsync();
        }
    }
}
