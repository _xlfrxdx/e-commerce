﻿using Domain.Ejemplo;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositorios
{
    public class ProductosRepositorio
    {
        private readonly ApplicationDbContext _context;
        public ProductosRepositorio(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<List<Producto>> ObtenerLista()
        {
            return await _context.Productos.Include(x=>x.ImagenProducto).Include(c=> c.Categoria).ToListAsync();
        }
        public async Task<List<Producto>> ObtenerListaPDescuento()
        {
            return await _context.Productos.Where(x => x.Descuento == true).Include(y => y.ImagenProducto).ToListAsync();
        }
        public async Task<List<Producto>> ObtenerListaPRecientes()
        {
            return await _context.Productos.OrderByDescending(x => x.Id).Include(y => y.ImagenProducto).Take(10).ToListAsync();
                        
            //return await _context.Productos.ToListAsync();
        }
        public async Task<List<Producto>> ObtenerListaxCategorias(int id)
        {
            return await _context.Productos.Include(x => x.Categoria).Include(z=> z.ImagenProducto).Where(x => x.Categoria.Id == id).ToListAsync();
        }
        public async Task<List<Producto>> ObtenerListaPMasVistos()
        {
            return await _context.Productos.OrderByDescending(x => x.ContadorVisitas).Include(y => y.ImagenProducto).Take(10).ToListAsync();
        }
        public async Task<int> Crear(Producto producto)
        {
            await _context.Productos.AddAsync(producto);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> Editar(Producto producto)
        {
            _context.Productos.Update(producto);
            return await _context.SaveChangesAsync();
        }
        public async Task<int> Eliminar(int id)
        {
            Producto producto = new Producto();
            producto = await _context.Productos.FirstOrDefaultAsync(x=> x.Id == id);
            _context.Remove(producto);
            return await _context.SaveChangesAsync();
        }
        public async Task<Producto> BuscarPorId(int id)
        {
            return await _context.Productos.Include(x=> x.Categoria).Include(y => y.ImagenProducto).FirstOrDefaultAsync(x => x.Id == id);
        }
        public async Task<bool> ExisteNombre(string nombre)
        {
            return await _context.Productos.AnyAsync( x => x.Nombre.ToLower() == nombre.ToLower());
        }
        public async Task<bool> ExisteNombreOtroId(string nombre, int id)
        {
            return await _context.Productos.AnyAsync(x => x.Nombre.ToLower() == nombre.ToLower() && x.Id != id);
        }




    }
}
