﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositorios
{
    public class ApplicationUserRepositorio
    {
        private readonly ApplicationDbContext _context;
        public ApplicationUserRepositorio(ApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<int> Eliminar(string id)
        {
            var appp = await _context.ApplicationUsers.FindAsync(id);
            _context.ApplicationUsers.Remove(appp);
            return await _context.SaveChangesAsync();
        }
    }
}
