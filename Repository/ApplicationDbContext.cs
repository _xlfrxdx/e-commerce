﻿using Audit.EntityFramework;
using Domain.AspNet;
using Domain.AuditDomain;
using Domain.Ejemplo;
using Domain.Objetos;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Repository
{
    [AuditDbContext(Mode = AuditOptionMode.OptOut, IncludeEntityObjects = true, AuditEventType = "{dbMaster}_{ApplicationDbContext}")]
    public class ApplicationDbContext : AuditIdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }
        public virtual DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public virtual DbSet<EventAudit> EventAudits { get; set; }
        public virtual DbSet<AspNetRole> ApplicationRoles { get; set; }
        public virtual DbSet<Categoria> Categorias { get; set; }
        public virtual DbSet<HistorialLogin> HistorialLogins { get; set; }
        public virtual DbSet<Archivo> ArchivosGenerales { get; set; }
        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<ImagenProducto> ImagenProductos { get; set; }
        public virtual DbSet<Producto> Productos { get; set; }
        public virtual DbSet<Persona> Personas { get; set; }
        public virtual DbSet<Venta> Ventas { get; set; }

        //public virtual DbSet<HistorialLogin> HistorialLogins { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //Tablas con Fluent Api

            //Filtros
            builder.Entity<ApplicationUser>().HasQueryFilter(x => !x.IsDeleted);
            builder.Entity<Producto>().HasQueryFilter(x => !x.IsDeleted);
            builder.Entity<Categoria>().HasQueryFilter(x => !x.IsDeleted);
            // builder.Entity<HistorialLogin>().HasQueryFilter(x => !x.IsDeleted);

        }
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaveChanges();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public async override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            OnBeforeSaveChanges();
            var result = await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
            return result;
        }
        private void OnBeforeSaveChanges()
        {
            try
            {
                foreach (var entry in ChangeTracker.Entries())
                {

                    foreach (var property in entry.Properties)
                    {

                        switch (entry.State)
                        {
                            case EntityState.Added:
                                entry.CurrentValues["IsDeleted"] = false;
                                entry.CurrentValues["RowVersion"] = DateTime.Now;
                                break;

                            case EntityState.Deleted:
                                entry.CurrentValues["IsDeleted"] = true;
                                entry.CurrentValues["RowVersion"] = DateTime.Now;
                                entry.State = EntityState.Modified;
                                break;

                            case EntityState.Modified:
                                if (property.IsModified)
                                {
                                    entry.CurrentValues["RowVersion"] = DateTime.Now;
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception e)
            {

                Debug.WriteLine(e.Message);
            }
        }
    }
}
