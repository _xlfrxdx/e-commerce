﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaseWeb_PFIncorporation.Controllers.Cliente;
using BaseWeb_PFIncorporation.Models.ViewModel;
using Domain.AspNet;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NLog.Layouts;
using Services.IServices;
using WebBase_PFIncorporation.Controllers;

namespace BaseWeb_PFIncorporation.Controllers.Seguridad
{
    [Authorize]
    [Layout("_AuthLayout")]
    public class AccountController : Controller
    {
        private readonly IHistorialLoginServices _historialLogin;
        private readonly ILogger _logger;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;
        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IHistorialLoginServices historialL, ILogger<AccountController> logger)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _historialLogin = historialL;
        }
        // GET: Account
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnURL = null)
        {
            HistorialLogin historial = new HistorialLogin
            {
                Username = !string.IsNullOrEmpty(model.Email) ? model.Email : "dirección email vacia",
                Fecha = DateTime.Now,
                DireccionIP = Request.Host.Value
            };
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    historial.Acceso = "Exitoso";
                    await _historialLogin.Crear(historial);
                    _logger.LogInformation("Usuario logged in.");
                    return RedirectToLocal(returnURL);
                }
                else
                {
                    historial.Acceso = "Fallido";
                    await _historialLogin.Crear(historial);
                    TempData["Error"] = "al iniciar sesión. Verificar el correo y contraseña.";
                   
                }
                if (result.Equals(Microsoft.AspNetCore.Identity.SignInResult.Failed))
                {
                    if (_userManager.FindByEmailAsync(model.Email).Result != null)
                    {
                        historial.Acceso = "Fallido";
                        _logger.LogWarning("Failed.");
                    }
                    else
                    {
                        historial.Acceso = "Usuario no registrado";
                        _logger.LogWarning("Usuario account is not allowed.");
                    }

                    await _historialLogin.Crear(historial);
                    return View(model);
                }
                historial.Acceso = "Fallido/Login-Invalido";
                await _historialLogin.Crear(historial);
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return View(model);
            }
            historial.Acceso = "Fallido/Error-ModelState";
            await _historialLogin.Crear(historial);
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: Account/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Account/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Account/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Account/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Account/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Account/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(InicioController.Index));
        }
    }
}