﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Domain.Ejemplo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.IServices;
using WebBase_PFIncorporation.Models;

namespace WebBase_PFIncorporation.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        
        public async Task<IActionResult> Index()
        {
           // string ola= "Olaaa soy cuy xddd";
            List<Categoria> Lista = new List<Categoria>();
           // Lista = await _categoriasServices.obtenerLista();
            return View(Lista);
        }
        public async Task<IActionResult> Crear()
        {
            Categoria categoria = new Categoria();
            return View(categoria);
        }
        //[HttpPost]
        //public async Task<IActionResult> Crear(Categoria categoria)
        //{
        //    if (ModelState.IsValid)
        //    {
              
        //        var response = await _categoriasServices.crear(categoria);
        //        if (response.Success)
        //        {
        //            TempData["Success"] = response.Message;
        //            return RedirectToAction("Index");
        //        }
        //        else
        //        {
        //            ModelState.AddModelError("modelError", response.Message);
        //            TempData["Error"] = response.Message;
        //            return View("Index");
        //        }
        //    }
        //    else
        //    {
        //        TempData["Error"] = "soy puto";
        //        return View("Index");
        //    }
        //}


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
