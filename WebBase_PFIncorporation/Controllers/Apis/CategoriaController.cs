﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Ejemplo;
using Microsoft.AspNetCore.Mvc;
using Services.IServices;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BaseWeb_PFIncorporation.Controllers.Apis
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        private readonly ICategoriasServices _categoriasServices;
        public CategoriaController(ICategoriasServices categorias)
        {
            _categoriasServices = categorias;
        }
        // GET: api/<CategoriaController>
        [HttpGet]
        public async Task<IActionResult> ObterLista()
        {
            return Ok(await _categoriasServices.obtenerLista());
        }

        // GET api/<CategoriaController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> BuscarPorId(int id)
        {
            return Ok(await _categoriasServices.buscarPorId(id));
        }


        // POST api/<CategoriaController>
        [HttpPost]
        public async Task<IActionResult> Crear(Categoria categoria)
        {
            if (!ModelState.IsValid)
                BadRequest();
            return Ok(await _categoriasServices.crear(categoria));
        }

        // PUT api/<CategoriaController>/5
        [HttpPut]
        public async Task<IActionResult> Update(Categoria categoria)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            return Ok(await _categoriasServices.editar(categoria));

        }

        // DELETE api/<CategoriaController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromBody]int id)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            return Ok(await _categoriasServices.eliminar(id));

        }
    }
}
