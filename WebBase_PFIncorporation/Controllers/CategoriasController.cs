﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Ejemplo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog.Layouts;
using Services.IServices;

namespace BaseWeb_PFIncorporation.Controllers
{
    [Layout("_Layout")]
    public class CategoriasController : Controller
    {
        private readonly ICategoriasServices _categoriasServices;
        public CategoriasController(ICategoriasServices categoriasServices)
        {
            _categoriasServices = categoriasServices;
        }

        // GET: CategoriasController
        public async Task<IActionResult> Index()
        {
            var lista = await _categoriasServices.obtenerLista();
            return View(lista);
        }

        // GET: CategoriasController/Crear
        [HttpGet]
        public IActionResult Crear()
        {
            Categoria categoria = new Categoria();
            return PartialView(categoria);
        }

        // POST: CategoriasController/Crear
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Crear(Categoria categoria)
        {
            if (ModelState.IsValid)
            {

                var response = await _categoriasServices.crear(categoria);
                if (response.Success)
                {
                    return Json(new { success = response.Success, msj = response.Message });
                }
                else
                {
                    ModelState.AddModelError("modelError", response.Message);
                    return Json(new { success = response.Success, msj = response.Message });
                }
            }
            return Json(new { success = false, msj = "¡Verifica que los datos esten completos y sean correctos, si el problema continua comunicate con el administrador!" });

        }

        // GET: CategoriasController/Edit/5
        public async Task<IActionResult> Editar(int id)
        {
            Categoria categoria = await _categoriasServices.buscarPorId(id);

            if (categoria == null)
            {
                return RedirectToAction("Index");
            }

            return PartialView(categoria);
        }

       // POST: CategoriasController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Editar(Categoria categoria)
        {
            if (ModelState.IsValid)
            {
                var response = await _categoriasServices.editar(categoria);
                if (response.Success)
                {
                    return Json(new { success = response.Success, msj = response.Message });
                }
                else
                {
                    ModelState.AddModelError("modelError", response.Message);
                    return Json(new { success = response.Success, msj = response.Message });
                }
            }
            return Json(new { success = false, msj = "¡Verifica que los datos esten completos y sean correctos, si el problema continua comunicate con el administrador!" });
        }

        // POST: CategoriasController/Delete/5
        public async Task<IActionResult> EliminarConfirmado(int id)
        {
            if (id <= 0) // Validamos que el id no sea nulo o que contenga un valor negativo.
            {
                return RedirectToAction("Index");
            }
            var response = await _categoriasServices.eliminar(id);
            if (response.Success) // Validamos sí el producto se eliminó.
            {
                return Json(new { success = response.Success, msj = response.Message });
            }
            else
            {
                ModelState.AddModelError("modelError", response.Message);
                return Json(new { success = response.Success, msj = response.Message });
            }
        }
    }
}
