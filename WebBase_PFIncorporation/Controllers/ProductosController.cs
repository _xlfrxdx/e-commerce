﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Ejemplo;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Services.IServices;

namespace BaseWeb_PFIncorporation.Controllers
{
    public class ProductosController : Controller
    {
        private readonly IProductoServices _productoServices;
        private readonly ICategoriasServices _categoriasServices;
        public ProductosController(IProductoServices productoServices, ICategoriasServices categoriasServices)
        {
            _productoServices = productoServices;
            _categoriasServices = categoriasServices;
        }
        // GET: ProductosController
        public async  Task<IActionResult> Index()
        {
            var lista = await  _productoServices.obtenerLista();
            return View(lista);
        }

        // GET: ProductosController/Details/5
        public async Task<IActionResult> Detalles(int id)
        {
            Producto producto = await _productoServices.buscarPorId(id);
            if (producto == null)
            {
                return RedirectToAction("Index");
            }
            return View(producto);
        }

        // GET: ProductosController/Create
        public async Task<IActionResult> Crear()
        {
            var categorias = await _categoriasServices.obtenerLista();
            ViewBag.Categorias= categorias.Any()? new  SelectList(categorias, "Id","Nombre"): null;
            return View();
        }

        // POST: ProductosController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async  Task<IActionResult> Crear(Producto producto)
        {
            if (ModelState.IsValid)
            {
                var response = await _productoServices.crear(producto);
                if (response.Success) // Validamos sí la unidad de medida se creó.
                {
                    //return Json(new { success = response.Success, msj = response.Message });
                    TempData["Success"] = response.Message;
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Error"] = response.Message;
                    return View(producto);
                }
            }
            TempData["Error"] = "¡Verifica que los datos esten completos y sean correctos, si el problema continua comunicate con el administrador!";
            return RedirectToAction("Index");
           

        }

        // GET: ProductosController/Edit/5
        public async Task<ActionResult> Editar (int Id)
        {
            var producto = await _productoServices.buscarPorId(Id);
            if (producto != null)
            {
                var categorias = await _categoriasServices.obtenerLista();
                ViewBag.Categorias = categorias.Any() ? new SelectList(categorias, "Id", "Nombre") : null;
                return View(producto);
            }
            return RedirectToAction("Index");
        }

        // POST: ProductosController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async  Task<ActionResult> Editar(Producto producto)
        {
            if (ModelState.IsValid)
            {
                var response = await _productoServices.editar(producto);
                if (response.Success) // Validamos sí la unidad de medida se creó.
                {
                    //return Json(new { success = response.Success, msj = response.Message });
                    TempData["Success"] = response.Message;
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Error"] = response.Message;
                    var categorias = await _categoriasServices.obtenerLista();
                    ViewBag.Categorias = categorias.Any() ? new SelectList(categorias, "Id", "Nombre") : null;
                    return View(producto);
                }
            }
            TempData["Error"] = "¡Verifica que los datos esten completos y sean correctos, si el problema continua comunicate con el administrador!";
            return RedirectToAction("Index");
        }

        // GET: ProductosController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ProductosController/Delete/5
        
        public async Task<IActionResult> EliminarConfirmado(int id)
        {
            if ( id <= 0) // Validamos que el id no sea nulo o que contenga un valor negativo.
            {
                return RedirectToAction("Index");
            }
            var response = await _productoServices.eliminar(id);
            if (response.Success) // Validamos sí el producto se eliminó.
            {
                return Json(new { success = response.Success, msj = response.Message });
            }
            else
            {
                ModelState.AddModelError("modelError", response.Message);
                return Json(new { success = response.Success, msj = response.Message });
            }
        }
        //[HttpGet]
        //public ActionResult ProductoIma(int Id)
        //{

        //}
        [HttpGet]
        public async Task<IActionResult> ProductoIma(int id)
        {
            ViewBag.id = id;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> ProductoIma(List<IFormFile> files, int Id)
        {
            if (ModelState.IsValid)
            {
                var response = await _productoServices.ProductosImagenes(files, Id);
                if (response.Success) // Validamos sí la unidad de medida se creó.
                {
                    //return Json(new { success = response.Success, msj = response.Message });
                    TempData["Success"] = response.Message;
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Error"] = response.Message;
                    var categorias = await _categoriasServices.obtenerLista();
                    ViewBag.Categorias = categorias.Any() ? new SelectList(categorias, "Id", "Nombre") : null;
                    return View();
                }
            }
            TempData["Error"] = "¡Verifica que los datos esten completos y sean correctos, si el problema continua comunicate con el administrador!";
            return RedirectToAction("Index");

        }
    }
}
