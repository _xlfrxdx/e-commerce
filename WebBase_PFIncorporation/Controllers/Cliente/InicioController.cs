﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NLog.Layouts;
using Services.IServices;

namespace BaseWeb_PFIncorporation.Controllers.Cliente
{
    
    [AllowAnonymous]
    [Layout("_HomeLayout")]
    public class InicioController : Controller
    {
        private readonly IProductoServices _productoServices;
        private readonly ICategoriasServices _categoriasServices;
        public InicioController(IProductoServices producto, ICategoriasServices categoriasServices)
        {
            _productoServices = producto;
            _categoriasServices = categoriasServices;
        }
        // GET: Inicio
        public async Task<IActionResult> Index()
        {
            var listas = await _productoServices.listaProductoViewModel();
            return View(listas);
        }
        public ActionResult ContactoCliente()
        {
            return View();
        }

        public async Task<IActionResult> Productos()
        {
            var categorias = await _categoriasServices.obtenerLista();
            ViewBag.Categorias = categorias.Any() ? new SelectList(categorias, "Id", "Nombre") : null;
            return View();
        }
        [HttpGet]
        public async Task<IActionResult> _ProductosCategorias(int id)
        {
            if (id <=0 )
            {
                var lista = await _productoServices.obtenerLista();
                return PartialView(lista);
            }
            else
            {
                var categorias = await _productoServices.ObtenerListaPCategoria(id);
                return PartialView(categorias);
            }
             

        }
        public async Task<IActionResult> Detalles(int id)
        {
            var producto = await _productoServices.buscarPorId(id);
            ViewBag.id = id;
            return View(producto);
        }

        // GET: Inicio/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var producto = await _productoServices.buscarPorId(id);
            return View(producto);
        }

        // GET: Inicio/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inicio/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Inicio/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Inicio/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Inicio/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Inicio/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}