﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Audit.Core;
using Audit.SqlServer;
using Audit.SqlServer.Providers;
using Domain.AspNet;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Repository;
using Services.IServices;
using Services.Services;
using Services.Services.Seguridad;

namespace WebBase_PFIncorporation
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        private const string esMXCulture = "es-MX";

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Get the service provider to access the http context
            var svcProvider = services.BuildServiceProvider();
            // Configure Audit.NET
            Audit.Core.Configuration.Setup()
                .UseEntityFramework(x => x
                    .AuditTypeNameMapper(typeName => "Audit_" + typeName)
                    .AuditEntityAction((evt, ent, auditEntity) =>
                    {
                        // Get the current HttpContext 
                        var httpContext = svcProvider.GetService<IHttpContextAccessor>().HttpContext;
                        // Store the identity name on the "UserName" property of the audit entity
                        ((dynamic)auditEntity).UserName = httpContext.User?.Identity.Name;
                    }));

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("AlfredoConnection")));

            services.AddIdentity<ApplicationUser, AspNetRole>().AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();
            services.AddMemoryCache();
            services.AddSession();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
          
            
            services.AddTransient<ApplicationDBSeeder>();
           // services.AddTransient<IHistorialLoginServicio, HistorialLoginServicio>();
            services.AddTransient<IApplicationUserServices, AplicationUserServicio>();
            services.AddTransient<ICategoriasServices, CategoriaServices>();
            services.AddTransient<IHistorialLoginServices, HistorialLoginServices>();
            services.AddTransient<IProductoServices, ProductoServices>();
            //services.AddTransient<ApplicationUserServicio>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ApplicationDBSeeder dbSeeder)
        {

            Audit.Core.Configuration.DataProvider = new SqlDataProvider()
            {

                ConnectionString = Configuration.GetConnectionString("AlfredoConnection"),
                Schema = "dbo",
                TableName = "EventAudit",
                IdColumnName = "EventId",
                JsonColumnName = "JsonData",
                LastUpdatedDateColumnName = "LastUpdatedDate",
                CustomColumns = new List<CustomColumn>()
                {
                    new CustomColumn("EventType", ev => ev.EventType),
                    //new CustomColumn("TargetOld", ev => ev.Target.SerializedOld),
                    //new CustomColumn("TargetNew", ev => ev.Target.SerializedNew),
                    //new CustomColumn("TargetType", ev => ev.Target.Type),
                    new CustomColumn("EndDate", ev => ev.EndDate),
                    new CustomColumn("StartDate", ev => ev.StartDate),
                    new CustomColumn("Duration", ev => ev.Duration),
                    new CustomColumn("Domain", ev => ev.Environment.DomainName),
                }
            };

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            if (!env.IsProduction())
            {
                // Ensure we have the default user added to the store
                dbSeeder.EnsureSeed().GetAwaiter().GetResult();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            var supportedCultures = new[]
    {
                new CultureInfo (esMXCulture)
            };
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(esMXCulture),
                // Formatting numbers, dates, etc.
                SupportedCultures = supportedCultures,
                // UI strings that we have localized.
                SupportedUICultures = supportedCultures
            });
            app.UseAuthentication();
            app.UseSession();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Inicio}/{action=Index}/{id?}");
            });
        }
    }
}
