﻿var validatorRecuperaPwd;

$(function () {
    validatorRecuperaPwd = $("#formRecuperaPwd").validate({
        lang: "es",
        errorPlacement: function (error, element) {
            $(element).closest("div").find(".text-danger").append(error);
        }
    });
});


$("#btnOlvidaste").on("click", function () {
    $("#loginContainer").hide();
    $("#recuperarPwdContainer").show();
});

$("#btnRegresarLogin").on("click", function () {
    $("#recuperarPwdContainer").hide();
    $("#loginContainer").show();
});

$("#btnRecuperarPwd").on("click", function () {
    if ($("#formRecuperaPwd").valid()) {
        $.ajax({
            data: {
                Email: $("#formRecuperaPwd input[name=email]").val()
            },
            title: 'Correo de confirmación enviado',
            type: 'POST',
            url: "/Account/ForgotPassword",
            dataType: 'json',
            beforeSend: function () {
                KApp.block('#recuperarPwdContainer', {});
            },
            success: function (data) {
                if (data.status) {
                    Swal.fire(
                        data.mensaje,
                        "Por favor, revise su correo electrónico para seguir con el proceso de recuperación de contraseña.",
                        'success'
                    );

                }
                else {
                    validatorRecuperaPwd.showErrors({
                        "email": data.mensaje
                    });
                }
                KApp.unblock('#recuperarPwdContainer');

            },
            error: function (ex) {
                Swal.fire(
                    'Error',
                    "Contacte a su administrador de sistema",
                    'error'
                );
                KApp.unblock('#recuperarPwdContainer');
            }
        });
    }

});

