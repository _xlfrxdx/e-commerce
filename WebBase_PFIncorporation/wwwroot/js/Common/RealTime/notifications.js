﻿

$(document).ready(function () {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    CargarLista();

    let connection = new signalR.HubConnectionBuilder().withUrl("/signalServer").build();

    connection.on('displayNotification', () => {
        $('#listNotification').empty();
        CargarLista();
    });

    connection.start();
});

function CargarLista() {

    $.ajax({
        url: "/api/Notificaciones/ObtenerNotificaciones",
        method: "GET",
        dataType: "json",
        success: function (result) {
            console.log(result);

            var notifications = result;
           
            var numero;
            //var numero = 0;
            notifications.forEach(function (notificacion, index) {
               
                if (notificacion.aceptada === false) {
                    if (notificacion.tipo === "info") {
                        toastr.info(notificacion.texto, notificacion.titulo);
                    } else if (notificacion.tipo === "success") {
                        toastr.success(notificacion.texto , notificacion.titulo);
                    } else if (notificacion.tipo === "error") {
                        toastr.error(notificacion.texto, notificacion.titulo);
                    } else if (notificacion.tipo === "warning") {
                        toastr.warning(notificacion.texto, notificacion.titulo);
                        
                    } else{
                        toastr.info(notificacion.texto , notificacion.titulo);
                    }

                    
                }
                Check(notificacion.idNotificacionUsuario);
                numero = notificacion.contador;
                addAlert(notificacion.url, notificacion.titulo, notificacion.tipo, notificacion.fecha, notificacion.leido, numero, notificacion.idNotificacionUsuario);     
              
            $('#countAlert').empty();
            $('#countAlert').append(numero);

            });
           
        },
        error: function (error) {
            console.log(error);
        }
    });
}

function Check(id) {
    $.ajax({
        url: "/api/Notificaciones/Check?id=" + id,
        method: "put",
        dataType: "json",
        success: function (result) {
            console.log("chido");
        },
        error: function (error) {
            console.log("error");
        }
    });
}

function Leer(id, numero) {
    $.ajax({
        url: "/api/Notificaciones/Leer?id=" + id,
        type: 'put',
        dataType: "json",
        success: function (result) {
            console.log(result);
            let num;
            if (numero > 0 ) {
                num = numero - 1;
            }
            
            console.log(num);
            $('#countAlert').empty();
            $('#countAlert').append(num);
            
        },
        error: function (error) {
            console.log("error");
        }
    });
}


function addAlert(url, titulo, tipo, fecha, leido, numero, id) {
    var myalert = '';
    if (leido) {
        myalert = $('<a href="' + url + '" onclick=Leer(' + id + ',' + numero + ')  " " class="k-notification__item"><div class="k-notification__item-icon"><i class="flaticon2-chart2 k-font-' + tipo + '"></i></div><div class="k-notification__item-details"><div class="k-notification__item-title">' + titulo + '</div><div class="k-notification__item-time">' + fecha + '</div></div></a>');
    } else {
        myalert = $('<a href="' + url + '" onclick=Leer(' + id + ',' + numero + ')  " " class="k-notification__item"><div class="k-notification__item-icon"><i class="flaticon2-chart2 k-font-' + tipo + '"></i></div><div class="k-notification__item-details"><div class="k-notification__item-title"><b>' + titulo + '</b></div><div class="k-notification__item-time">' + fecha + '</div></div></a>');

    }
    $('#listNotification').append(myalert);

}

