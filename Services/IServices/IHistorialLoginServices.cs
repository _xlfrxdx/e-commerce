﻿using Domain.AspNet;
using Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.IServices
{
   public interface IHistorialLoginServices
    {
        Task<ResponseHelper> Crear(HistorialLogin historialLogin);
    }
}
