﻿using Domain.Ejemplo;
using Domain.Util;
using Domain.ViewModel;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.IServices
{
   public  interface IProductoServices
    {
        Task<List<Producto>> obtenerLista();
        Task<ResponseHelper> crear(Producto producto);
        Task<ResponseHelper> editar(Producto producto);
        Task<ResponseHelper> eliminar(int id);
        Task<Producto> buscarPorId(int id);
        Task<ResponseHelper> ProductosImagenes(List<IFormFile> ListaImagenes, int id);
        Task<ListaProductoViewModel> listaProductoViewModel();
        Task<List<Producto>> ObtenerListaPCategoria(int id);
        //Task<bool> ExisteNombreDiferenteId(string nombre, int id);
    }
}
