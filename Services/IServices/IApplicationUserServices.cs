﻿using Domain.Util;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.IServices
{
    public interface IApplicationUserServices
    {
        Task<ResponseHelper> Eliminar(string id);
    }
}
