﻿using Domain.Ejemplo;
using Domain.Util;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.IServices
{
    public interface ICategoriasServices
    {
        Task<List<Categoria>> obtenerLista();
        Task<ResponseHelper> crear(Categoria categoria);
        Task<ResponseHelper> editar(Categoria categoria);
        Task<ResponseHelper> eliminar(int id);
        Task<Categoria> buscarPorId(int? id);
        Task<bool> ExisteNombreDiferenteId(string nombre, int id);
    }
}
