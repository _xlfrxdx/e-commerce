﻿using Domain.Ejemplo;
using Domain.Util;
using Microsoft.Extensions.Logging;
using Repository;
using Repository.Repositorios.Ejemplo;
using Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class CategoriaServices : ICategoriasServices
    {
        private readonly CategoriaRepositorio _categoriaRepositorio;
        private readonly ILogger _logger;
        public CategoriaServices(ApplicationDbContext context, ILogger<CategoriaServices> logger)
        {

            _categoriaRepositorio = new CategoriaRepositorio(context);
            _logger = logger;
        }
        public async Task<List<Categoria>> obtenerLista()
        {
            List<Categoria> Lista = new List<Categoria>();
            try
            {
                Lista = await _categoriaRepositorio.obtenerLista();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return Lista;
        }

        public async Task<Categoria> buscarPorId(int? id)
        {
            Categoria categoria = new Categoria();
            try
            {
                categoria = await _categoriaRepositorio.buscarPorId(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return categoria;
        }

        public async Task<bool> ExisteNombreDiferenteId(string nombre, int id)
        {    
            return await _categoriaRepositorio.ExisteNombreDiferenteId(id, nombre);        
        }

        public async Task<ResponseHelper> crear(Categoria categoria)
        {
            ResponseHelper response = new ResponseHelper();
            try
            {
                if (categoria == null)
                {
                    response.Message = $"Error al intentar crear la categoria con el nombre {categoria.Nombre}";
                    response.Success = false;
                }
                else
                {
                    var result = await _categoriaRepositorio.crear(categoria);
                    if (result > 0)
                    {
                        response.Message = $"Exito al crear la categoria con el nombre {categoria.Nombre}";
                        response.Success = true;
                    }
                    else
                    {
                        response.Message = $"Error al intentar crear la categoria con el nombre {categoria.Nombre}";
                        response.Success = false;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                response.Message = $"Error al intentar crear la categoria con el nombre {categoria.Nombre}";
                response.Success = false;
            }
            return response;
        }

        public async Task<ResponseHelper> editar(Categoria categoria)
        {
            ResponseHelper response = new ResponseHelper();
            try
            {
                if (categoria == null)
                {
                    response.Message = $"Error al intentar editar la categoria con el nombre {categoria.Nombre}";
                    response.Success = false;
                }
                else
                {
                    var cate = await _categoriaRepositorio.buscarPorId(categoria.Id);
                    cate.Nombre = categoria.Nombre;
                    cate.Descripcion = categoria.Descripcion;
                    if (await _categoriaRepositorio.editar(cate) > 0)
                    {
                        response.Message = $"Exito al editar la categoria con el nombre {cate.Nombre}";
                        response.Success = true;
                    }
                    else
                    {
                        response.Message = $"Error al intentar editar la categoria con el nombre {cate.Nombre}";
                        response.Success = false;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                response.Message = $"Error al intentar editar la categoria con el nombre {categoria.Nombre}";
                response.Success = false;
            }
            return response;
        }

        public async Task<ResponseHelper> eliminar(int id)
        {
            ResponseHelper response = new ResponseHelper();
            try
            {
                var cate = await _categoriaRepositorio.buscarPorId(id);
                if (cate== null)
                {
                    response.Message = $"Error al intentar eliminar la categoria";
                    response.Success = false;
                }
                else
                {
                    if (await _categoriaRepositorio.Eliminar(id) > 0)
                    {
                        response.Message = $"Exito al eliminar la categoria con el nombre{cate.Nombre}";
                        response.Success = true;
                    }
                    else
                    {
                        response.Message = $"Error al intentar eliminar la categoria con el nombre{cate.Nombre}";
                        response.Success = false;
                    }

                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return response;
        }
    }
}
