﻿using Domain.Ejemplo;
using Domain.Objetos;
using Domain.Util;
using Domain.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Repository;
using Repository.Repositorios;
using SendGrid;
using Services.IServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
   public  class ProductoServices : IProductoServices
    {
        private readonly ProductosRepositorio _productosRepositorio;
        private readonly ImagenProductoRepositorio _imagenProductoRepositorio; 
        private readonly ILogger _logger;
        public ProductoServices(ApplicationDbContext context, ILogger<ProductoServices> logger )
        {
            _productosRepositorio = new ProductosRepositorio(context);
            _imagenProductoRepositorio = new ImagenProductoRepositorio(context);
            _logger = logger;
        }
        public async Task<Producto> buscarPorId(int id)
        {
            Producto producto = new Producto();
            try
            {
                producto = await _productosRepositorio.BuscarPorId(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return producto;
        }

        public async Task<ResponseHelper> crear(Producto producto)
        {
            ResponseHelper response = new ResponseHelper();
            try
            {
                if (await _productosRepositorio.ExisteNombre(producto.Nombre))
                {
                    response.Message = $"Lo sentimos, el nombre: {producto.Nombre} que intenta guardar ya se encuentra registrado, pruebe con uno nuevo.";
                    response.Success = false;
                }
                else
                {
                    producto.FechaRegistro = DateTime.Now;
                    if (await _productosRepositorio.Crear(producto)>0)
                    {
                        response.Message = $"Exito al crear el producto con el nombre: {producto.Nombre}";
                        response.Success = true;
                        _logger.LogInformation(response.Message);
                    }
                    else
                    {
                        response.Message = $"Error al crear el producto con el nombre: {producto.Nombre}";
                        response.Success = false;
                        _logger.LogInformation(response.Message);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return response;
        }

        public async  Task<ResponseHelper> ProductosImagenes(List<IFormFile> ListaImagenes, int id)
        {
            ResponseHelper response = new ResponseHelper();
            try
            {
                var producto = await _productosRepositorio.BuscarPorId(id);
                var lista = await _imagenProductoRepositorio.BuscarImagenXPorducto(id);
                if (lista.Count == 0 && producto!= null)
                {
                    string CarpetaIMG = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Documentos\\Productos\\" + producto.Nombre);
                    List<ImagenProducto> imagenP = new List<ImagenProducto>();
                    //ImagenProducto ImgPro = new ImagenProducto();
                    if (!Directory.Exists(CarpetaIMG))//Valida si existe el directorio
                    {
                        Directory.CreateDirectory(CarpetaIMG);
                    }
                    int Contador= 0;
                    foreach (var item in ListaImagenes)
                    {
                        if(item != null)
                        {
                            var nombresito = producto.Id + "-" + producto.Nombre + "_Imagenes-" + Contador + Path.GetExtension(item.FileName);
                            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot\\Documentos\\Productos\\" + producto.Nombre, nombresito);
                            if (File.Exists(path))//Valida si el archivo ya existe
                            {
                                File.Delete(path);
                            }
                            if (item.Length > 0)//Valida si el documento no esta vacio
                            {
                                using (var stream = new FileStream(path, FileMode.Create))
                                {
                                    item.CopyTo(stream);
                                }
                                imagenP.Add(
                                    new ImagenProducto()
                                    {
                                        Nombre = nombresito,
                                        IdProducto = producto.Id,
                                        URL = "\\Productos\\" + producto.Nombre + "\\" + nombresito,
                                        Estatus = true,
                                        AppUser = "Por el momento es este texto",
                                        FechaDeAlta = DateTime.Now,
                                        IsDeleted= false,
                                        RowVersion= DateTime.Now,
                                        Contador = Contador,
                                    }
                                    );
                            }
                        }
                        Contador++;
                        
                    }
                    if (await _imagenProductoRepositorio.CrearLista(imagenP) > 0)
                    {
                        response.Success = true;
                        response.Message = $"Se han creado satisfactoriamente la lista de imagen del producto: {producto.Nombre}";
                        _logger.LogError(response.Message);

                    }
                    else
                    {
                        response.Success = false;
                        response.Message =$"Error al intentar crear el listado de imagenes en del producto: {producto.Nombre}";
                        _logger.LogError(response.Message);
                    }

                }
                else
                {

                }


            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
               
            }
            return response;
        }

        public async Task<ResponseHelper> editar(Producto producto)
        {
            ResponseHelper response = new ResponseHelper();
            try
            {
                if (await _productosRepositorio.ExisteNombreOtroId(producto.Nombre, producto.Id))
                {
                    response.Message = $"Lo sentimos, el nombre: {producto.Nombre} que intenta guardar ya se encuentra registrado, pruebe con uno nuevo.";
                    response.Success = false;
                }
                else
                {
                    producto.AppUser = "Por el momento sera este texto";
                    producto.RowVersion = DateTime.Now;
                    
                    if (await _productosRepositorio.Editar(producto)>0)
                    {
                        response.Message = $"Exito al ediatr el producto con el nombre: {producto.Nombre}";
                        response.Success = true;
                        _logger.LogInformation(response.Message);
                    }
                    else
                    {
                        response.Message = $"Error al editar el producto con el nombre: {producto.Nombre}";
                        response.Success = false;
                        _logger.LogInformation(response.Message);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return response;
        }

        public async Task<ResponseHelper> eliminar(int id)
        {
            ResponseHelper response = new ResponseHelper();
            try
            {
                if ( id <= 0)
                {
                    response.Message = $"Lo sentimos, No puede eliminar el producto.";
                    response.Success = false;
                    _logger.LogInformation(response.Message);
                }
                else
                {
                    if (await _productosRepositorio.Eliminar(id)>0)
                    {
                        response.Message = $"Producto eliminado con éxito";
                        response.Success = true;
                        _logger.LogInformation(response.Message);
                    }
                    else
                    {
                        response.Message = $"Error al intentar eliminar el producto.";
                        response.Success = false;
                        _logger.LogInformation(response.Message);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                response.Success = false;
            }
            return response;
        }

        public async Task<List<Producto>> obtenerLista()
        {
            List<Producto> lista = new List<Producto>();
            try
            {
                lista = await _productosRepositorio.ObtenerLista();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return lista;
        }

        public async Task<ListaProductoViewModel> listaProductoViewModel()
        {
            ListaProductoViewModel listaProductoView = new ListaProductoViewModel();
            try
            {
                listaProductoView.ObtenerListaPDescuento = await _productosRepositorio.ObtenerListaPDescuento();
                listaProductoView.ObtenerListaPMasVistos = await _productosRepositorio.ObtenerListaPMasVistos();
                listaProductoView.ObtenerListaPRecientes = await _productosRepositorio.ObtenerListaPRecientes();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
            }
            return listaProductoView;
        }

        public async  Task<List<Producto>> ObtenerListaPCategoria(int id)
        {
            List<Producto> ListaProductoCatergorias = new List<Producto>();
            try
            {
                ListaProductoCatergorias = await _productosRepositorio.ObtenerListaxCategorias(id);
            }
            catch (Exception e)
            {

                _logger.LogError(e.Message);
            }
            return ListaProductoCatergorias;
        }
    }
}
