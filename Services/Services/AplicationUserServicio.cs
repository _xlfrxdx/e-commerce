﻿using Domain.AspNet;
using Domain.Util;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Repository;
using Repository.Repositorios;
using Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
    public class AplicationUserServicio : IApplicationUserServices
    {
        private readonly ApplicationUserRepositorio _applicationUserRepository;
        private readonly IHttpContextAccessor _accessor;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger _logger;
        public AplicationUserServicio(ApplicationDbContext context, ILogger<ApplicationUser> logger, IHttpContextAccessor accessor, UserManager<ApplicationUser> userManager)
        {
            _applicationUserRepository = new ApplicationUserRepositorio(context);
            _logger = logger;
            _accessor = accessor;
            _userManager = userManager;
        }
        public Task<ResponseHelper> Eliminar(string id)
        {
            throw new NotImplementedException();
        }
    }
}
