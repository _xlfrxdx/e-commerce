﻿using Domain.AspNet;
using Domain.Util;
using Microsoft.Extensions.Logging;
using Repository;
using Repository.Repositorios.Seguridad;
using Services.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services.Seguridad
{
    public class HistorialLoginServices : IHistorialLoginServices
    {
        private readonly HistorialLoginRepositorio _historialLoginRepositorio;
        private readonly ILogger _logger;
        public HistorialLoginServices(ApplicationDbContext context, ILogger<HistorialLoginRepositorio> logger)
        {
            _historialLoginRepositorio = new HistorialLoginRepositorio(context);
            _logger = logger;
        }
        public async Task<ResponseHelper> Crear(HistorialLogin historialLogin)
        {
            var response = new ResponseHelper();
            try
            {
                if (historialLogin == null)
                {
                    response.Success = false;
                    response.Message = "Error al crear el registro";
                    _logger.LogInformation(response.Message);
                }
                else
                {
                    if (await _historialLoginRepositorio.Crear(historialLogin) > 0)
                    {
                        response.Success = true;
                        response.Message = "Creación de registro exitoso ";
                       _logger.LogInformation(response.Message);
                    }
                    else
                    {
                        response.Success = false;
                        response.Message = "Error al crear el registro";
                        _logger.LogInformation(response.Message);
                    }
                }
               

            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Error al crear el registro";
                _logger.LogInformation(e.Message);
            }
            return response;
        }
    }
}
